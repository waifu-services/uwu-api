const RateLimiter = require('../../../structures/RateLimiter');

class uwuGET {
	constructor(controller) {
		this.path = '/uwu';
		this.router = controller.router;
		this.database = controller.database;

		this.rateLimiter = new RateLimiter({ max: 10 }); // 10/10 limit

		this.router.get(
			this.path,
			this.rateLimiter.limit.bind(this.rateLimiter),
			this.run.bind(this)
		);
	}

	async run(req, res) {
        if (!req.query.text) return res.status(200).send({ msg: "You need to provide text params." });
        let text = req.query.text

        text = text.replace(/([lr])/g, "w");
		text = text.replace(/([aeiou])/ig, "w$1");

		return res.status(200).send({ text: text });
	}
}

module.exports = uwuGET;
