const express = require('express'),
	app = express(),
	bodyParser = require('body-parser'),
	toml = require('toml'),
	fs = require('fs'),
	settings = toml.parse(fs.readFileSync('./settings.toml'));

app.set('trust proxy', 'loopback');
app.set('env', 'production');
app.disable('x-powered-by');

// Parse data into req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// In dev we need a way to get images
if (process.env.NODE_ENV === 'development') {
	app.use('/image', express.static('image', { index: false, extensions: ['jpg'] }));
	app.use('/thumbnail', express.static('thumbnail', { index: false, extensions: ['jpg'] }));
}

app.use((req, res, next) => {
	res.set('Access-Control-Allow-Origin', '*');
	res.set('Access-Control-Allow-Credentials', 'true');
	res.set('Access-Control-Allow-Methods', 'GET,OPTIONS,POST,PUT,PATCH,DELETE');
	res.set('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization');
	return next();
});

// Load in the routes for express
let apiv1 = new (require('./api/v1/Router.js'))(settings);
app.use(apiv1.path, apiv1.router);

// Start the express server
app.listen(settings.port, error => {
	if (error)
		return console.log(error)
	console.log('Server online');
});
process.on('unhandledRejection', err => console.log(err));